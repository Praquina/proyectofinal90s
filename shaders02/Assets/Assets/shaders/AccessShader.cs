﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccessShader : MonoBehaviour {
    [SerializeField] private SkinnedMeshRenderer player;
    private float positionValue = 0;

	// Use this for initialization
	void Start () {
        print(player.material.GetFloat("Vector1_F554B9AE"));
		
	}
	
	// Update is called once per frame
	void Update () {
        positionValue += 0.001f;
        player.material.SetFloat("Vector1_F554B9AE", positionValue);
	}
}
